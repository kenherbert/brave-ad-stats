/* global $ */

//FUTURE: Test if user is on Brave - if not show a referral link for Brave

const adServerURI = 'https://ads-static.brave.com/v9/catalog';

const conf = {
    method: 'GET',
    mode: 'cors'
};


let minValue = null;
let maxValue = null;
let averageValue = null;


/**
 * Check if elements need dark mode styling
 */
function respectDarkMode() {
    if($('body').hasClass('dark-mode')) {
        $('#ad-catalog-card, #pending-rewards, #ads-received').addClass('bg-dark text-white');
        $('#ad-catalog-table').addClass('table-dark');
    } else {
        $('#ad-catalog-card, #pending-rewards, #ads-received').removeClass('bg-dark text-white');
        $('#ad-catalog-table').removeClass('table-dark');
    }
}



/**
 * Truncate a number to a fixed length, or less if there are trailing zeroes
 * @param {Number} value The value to truncate
 */
function truncateNumber(value) {
    return parseFloat(value.toFixed(minValue.toString().length - 2));
}


/**
 *
 * @param {Object} catalog The ad catalog object
 */
function populateAdCatalogData(catalog) {
    let regions = new Set();
    let values = [];
    let campaignsByValue = [];
    let totalAdvertisers = new Set();
    let totalCampaigns = 0;
    let totalAds = 0;
    let filter = $('input[name="filter"]:checked').val();


    catalog.campaigns.forEach(campaign => {
        let now = new Date().toISOString();

        if(campaign.startAt < now && campaign.endAt > now) {
            regions.add(...campaign.geoTargets.map(geoTarget => geoTarget.name));

            campaign.creativeSets.forEach((creativeSet, index) => {
                const value = parseFloat(creativeSet.value);

                if(filter === 'all' || creativeSet.oses.length === 0 || creativeSet.oses.some(os => os.name === filter)) {
                    if(index === 0) {
                        totalCampaigns += 1;
                    }

                    totalAdvertisers.add(campaign.advertiserId);

                    if(campaignsByValue[value.toString()]) {
                        if(index === 0) {
                            campaignsByValue[value.toString()].campaigns += 1;
                        }

                        campaignsByValue[value.toString()].ads += creativeSet.creatives.length;
                        campaignsByValue[value.toString()].advertisers.add(campaign.advertiserId);
                    } else {
                        let advertisers = new Set();
                        advertisers.add(campaign.advertiserId);
                        campaignsByValue[value.toString()] = {value: value, advertisers: advertisers, campaigns: 1/*, creativeSets: 1*/, ads: creativeSet.creatives.length};
                    }

                    totalAds += creativeSet.creatives.length;

                    values.push(...Array(creativeSet.creatives.length).fill(value));
                }
            });
        }
    });
    campaignsByValue = Object.values(campaignsByValue);
    campaignsByValue.sort((a, b) => a.value - b.value);

    values.sort();

    minValue = Math.min(...values);
    maxValue = Math.max(...values);
    averageValue = values.reduce((a, b) => a + b, 0) / values.length;

    let output = campaignsByValue.map(obj => `<tr><td>${obj.value}</td><td>${obj.advertisers.size}</td><td>${obj.campaigns}</td><td>${obj.ads}</td></tr>`).join();

    $('#ad-catalog').html(output);

    $('#advertiser-total').text(totalAdvertisers.size);
    $('#campaign-total').text(totalCampaigns);
    $('#ad-total').text(totalAds);

    $('#ad-region').text(`${Array.from(regions).sort().join(', ')}`);
    if(regions.size > 1) {
        $('#multi-region-help')[0].style.display = 'inline';
    } else {
        $('#multi-region-help')[0].style.display = 'none';
    }
    $('#minimum-ad-value').text(`${minValue} BAT`);
    $('#average-ad-value').text(`~${truncateNumber(averageValue)} BAT`);
    $('#maximum-ad-value').text(`${maxValue} BAT`);
}


/**
 * Load the ad catalog from Brave's server
 */
function getAdCatalog() {
    fetch(adServerURI, conf)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Response ${response.status}: ${response.statusText}`);
            }

            return response.json();
        })
        .then(data => {
            populateAdCatalogData(data);
        })
        .catch(error => {
            $('#ad-catalog').html('<tr><td colspan="4" class="text-danger" style="text-align: center;">Error - unable to load ad catalog</td></tr>');
            $('#ad-region').text('?');
            $('#minimum-ad-value').text('?');
            $('#average-ad-value').text('?');
            $('#maximum-ad-value').text('?');

            console.error('Unable to fetch ad catalog:', error);
        });
}


/**
 * Initialize the state of inputs
 */
function initialize() {
    respectDarkMode();
    $('[data-toggle="tooltip"]').tooltip();

    getAdCatalog();

    $('input[name="filter"]').click(getAdCatalog);

    setInterval(respectDarkMode, 1000);
}


initialize();
