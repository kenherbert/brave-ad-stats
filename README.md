# Brave Ad Stats

An overview of the Brave ad catalog for a user's region, showing a breakdown of the available campaigns & ads by BAT per ad, with a calculated average BAT per ad.

Please note that this was made for a section of the HTML code to be copy/pasted into a flatpage in my [Django portfolio](https://gitlab.com/kenherbert/developer-portfolio) but is setup as a standalone page with no requirement for Python or Django for completeness.